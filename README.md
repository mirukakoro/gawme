# GaWMe

Strazoloid-based window manager. This is not a normal wm. It is not X / Wayland-based. It runs off of Pygame, and there are plans to adopt compatibility for normal X / Wayland based wm support, so normal GTK, Qt, etc windows can appear on GaWMe.

# Screenshots

![GaWMe Screenshot 1](https://i.imgur.com/NUzqCOa.png)